package com.agribazaar.lab;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.agribazaar.lab.Model.Commodity;
import com.agribazaar.lab.Model.CommodityModel;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteArrayAdapter extends ArrayAdapter<CommodityModel> {

    private ArrayList<CommodityModel> countryListFull;

    public AutoCompleteArrayAdapter(@NonNull Context context, int textViewResourceId, @NonNull ArrayList<CommodityModel> countryList) {
        super(context, 0, textViewResourceId, countryList);

        countryListFull = new ArrayList<>(countryList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return countryFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.country_autocomplete_row,parent,false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.text_view_name);


        CommodityModel countryItem = getItem(position);

        textViewName.setText(countryItem.getComName());

        return convertView;
    }

    public CommodityModel getSelectedItem(int position){
        return countryListFull.get(position);
    }

    private Filter countryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            FilterResults results = new FilterResults();

            List<CommodityModel> suggestions = new ArrayList<>();

            if(charSequence == null || charSequence.length() == 0){

                suggestions.addAll(countryListFull);

            } else {

                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (CommodityModel item : countryListFull) {

                    if(item.getComName().toLowerCase().contains(charSequence)){

                        suggestions.add(item);

                    }
                }

            }

            results.values = suggestions;

            results.count =suggestions.size();

            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            clear();

            addAll((List)filterResults.values);

            notifyDataSetChanged();

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {

            return ((CommodityModel) resultValue).getComName();
        }
    };
}
