package com.agribazaar.lab;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.agribazaar.lab.Model.CommodityModel;
import com.agribazaar.lab.Model.ParametersModel;

import java.util.ArrayList;
import java.util.List;

public class ParameterAutoCompleteArrayAdapter extends ArrayAdapter<ParametersModel> {

    private ArrayList<ParametersModel> countryListFull;

    public ParameterAutoCompleteArrayAdapter(@NonNull Context context, int textViewResourceId, @NonNull ArrayList<ParametersModel> countryList) {
        super(context, 0, textViewResourceId, countryList);

        countryListFull = new ArrayList<>(countryList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return countryFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.country_autocomplete_row,parent,false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.text_view_name);


        ParametersModel countryItem = getItem(position);

        textViewName.setText(countryItem.getName());

        return convertView;
    }

    public ParametersModel getSelectedItem(int position){
        return countryListFull.get(position);
    }

    private Filter countryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            FilterResults results = new FilterResults();

            List<ParametersModel> suggestions = new ArrayList<>();

            if(charSequence == null || charSequence.length() == 0){

                suggestions.addAll(countryListFull);

            } else {

                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (ParametersModel item : countryListFull) {

                    if(item.getName().toLowerCase().contains(charSequence)){

                        suggestions.add(item);

                    }
                }

            }

            results.values = suggestions;

            results.count =suggestions.size();

            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            clear();

            addAll((List)filterResults.values);

            notifyDataSetChanged();

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {

            return ((ParametersModel) resultValue).getName();
        }
    };
}
