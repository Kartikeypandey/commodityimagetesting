package com.agribazaar.lab;

import android.util.Log;


/**
 * Created by Bashir on 21-Jul-16.
 */

public class Logger {
    private static String LOG_TAG = "DEBUG ";

    public static void e(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.e(tag, "" + message);
    }

    public static void e(String message) {
        e(LOG_TAG, message);
    }

    public static void d(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.d(tag, "" + message);
    }

    public static void d(String message) {
        d(LOG_TAG, message);
    }

    public static void i(String tag, String message) {
        if (BuildConfig.DEBUG) Log.i(tag, "" + message);
    }

    public static void i(String message) {
        i(LOG_TAG, message);
    }

    public static void w(String tag, String message) {
        if (BuildConfig.DEBUG) Log.w(tag, "" + message);
    }

    public static void LogError(Exception e) {
        e(e.getMessage());
    }

    public static void w(String message) {
        w(LOG_TAG, message);
    }
}
