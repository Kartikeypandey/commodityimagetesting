package com.agribazaar.lab;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Util {

    public static <T> T getModel(String text, Class<T> modelClass) {
        try {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            return gson.fromJson(text, modelClass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isValidText(String text) {
        if (text != null && !TextUtils.isEmpty(text.trim()) && !text.equals("null")) {
            return true;
        }
        return false;
    }
}
