package com.agribazaar.lab;


import com.agribazaar.lab.network.SharedPrefsHelper;

public class SharedPreferencesUtil {
    public static void save(String key, Object value) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        helper.save(key, value);
    }

    public static void delete(String key) {
        try {
            SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
            helper.remove(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(String key, T defValue) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        return helper.get(key);
    }

    public static int getInt(String key, int defValue) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        return helper.getInt(key,defValue);
    }

    public static boolean has(String key) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        return helper.has(key);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getValue(String key, T defValue) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        if (helper.has(key))
            return helper.get(key);
        return defValue;
    }

    @SuppressWarnings("unchecked")
    public static long getLongValue(String key, long defValue) {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
        if (helper.has(key))
            return helper.get(key);
        return defValue;
    }

    public static void clearAll() {
        SharedPrefsHelper helper = SharedPrefsHelper.getInstance();
//        helper.clearAll();
//        helper.clearAllPreferences();
    }
}
