package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ParametersModel implements Parcelable {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.id);
    }

    public ParametersModel() {
    }

    protected ParametersModel(Parcel in) {
        this.name = in.readString();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<ParametersModel> CREATOR = new Parcelable.Creator<ParametersModel>() {
        @Override
        public ParametersModel createFromParcel(Parcel source) {
            return new ParametersModel(source);
        }

        @Override
        public ParametersModel[] newArray(int size) {
            return new ParametersModel[size];
        }
    };
}
