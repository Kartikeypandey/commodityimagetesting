package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CommodityImage implements Parcelable {
    private String image_path;

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.image_path);
    }

    public CommodityImage() {
    }

    protected CommodityImage(Parcel in) {
        this.image_path = in.readString();
    }

    public static final Parcelable.Creator<CommodityImage> CREATOR = new Parcelable.Creator<CommodityImage>() {
        @Override
        public CommodityImage createFromParcel(Parcel source) {
            return new CommodityImage(source);
        }

        @Override
        public CommodityImage[] newArray(int size) {
            return new CommodityImage[size];
        }
    };
}
