package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CommodityImageResponse implements Parcelable {
    private CommodityImage data;
    private boolean error;
    private String msg;

    public CommodityImage getData() {
        return data;
    }

    public void setData(CommodityImage data) {
        this.data = data;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
        dest.writeByte(this.error ? (byte) 1 : (byte) 0);
        dest.writeString(this.msg);
    }

    public CommodityImageResponse() {
    }

    protected CommodityImageResponse(Parcel in) {
        this.data = in.readParcelable(CommodityImage.class.getClassLoader());
        this.error = in.readByte() != 0;
        this.msg = in.readString();
    }

    public static final Parcelable.Creator<CommodityImageResponse> CREATOR = new Parcelable.Creator<CommodityImageResponse>() {
        @Override
        public CommodityImageResponse createFromParcel(Parcel source) {
            return new CommodityImageResponse(source);
        }

        @Override
        public CommodityImageResponse[] newArray(int size) {
            return new CommodityImageResponse[size];
        }
    };
}
