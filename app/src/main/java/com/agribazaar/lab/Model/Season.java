package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Season implements Parcelable {

    private String season_name;


    public String getSeason_name() {
        return season_name;
    }

    public void setSeason_name(String season_name) {
        this.season_name = season_name;
    }

    public Season(String season_name) {
        this.season_name = season_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.season_name);
    }

    protected Season(Parcel in) {
        this.season_name = in.readString();
    }

    public static final Parcelable.Creator<Season> CREATOR = new Parcelable.Creator<Season>() {
        @Override
        public Season createFromParcel(Parcel source) {
            return new Season(source);
        }

        @Override
        public Season[] newArray(int size) {
            return new Season[size];
        }
    };
}
