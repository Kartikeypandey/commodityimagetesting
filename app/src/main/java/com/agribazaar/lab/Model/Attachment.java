package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abc on 10/26/2017.
 */

public class Attachment implements Parcelable {
    private int attachmentId;
    private String attachmentName;
    private String attachmentPath;
    private String attachmentMime;
    private int document_type;
    private String doc_path;
    private String doc_name;

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_path() {
        return doc_path;
    }

    public void setDoc_path(String doc_path) {
        this.doc_path = doc_path;
    }

    public int getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public String getAttachmentMime() {
        return attachmentMime;
    }

    public void setAttachmentMime(String attachmentMime) {
        this.attachmentMime = attachmentMime;
    }

    public int getDocument_type() {
        return document_type;
    }

    public void setDocument_type(int document_type) {
        this.document_type = document_type;
    }


    public Attachment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.attachmentId);
        dest.writeString(this.attachmentName);
        dest.writeString(this.attachmentPath);
        dest.writeString(this.attachmentMime);
        dest.writeInt(this.document_type);
        dest.writeString(this.doc_path);
        dest.writeString(this.doc_name);
    }

    protected Attachment(Parcel in) {
        this.attachmentId = in.readInt();
        this.attachmentName = in.readString();
        this.attachmentPath = in.readString();
        this.attachmentMime = in.readString();
        this.document_type = in.readInt();
        this.doc_path = in.readString();
        this.doc_name = in.readString();
    }

    public static final Creator<Attachment> CREATOR = new Creator<Attachment>() {
        @Override
        public Attachment createFromParcel(Parcel source) {
            return new Attachment(source);
        }

        @Override
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };
}
