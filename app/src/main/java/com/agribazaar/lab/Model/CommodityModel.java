package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CommodityModel implements Parcelable {

    private String comName;
     private String comId;
     private ArrayList<ParametersModel> parameters;

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public ArrayList<ParametersModel> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<ParametersModel> parameters) {
        this.parameters = parameters;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comName);
        dest.writeString(this.comId);
        dest.writeTypedList(this.parameters);
    }

    public CommodityModel() {
    }

    protected CommodityModel(Parcel in) {
        this.comName = in.readString();
        this.comId = in.readString();
        this.parameters = in.createTypedArrayList(ParametersModel.CREATOR);
    }

    public static final Parcelable.Creator<CommodityModel> CREATOR = new Parcelable.Creator<CommodityModel>() {
        @Override
        public CommodityModel createFromParcel(Parcel source) {
            return new CommodityModel(source);
        }

        @Override
        public CommodityModel[] newArray(int size) {
            return new CommodityModel[size];
        }
    };
}
