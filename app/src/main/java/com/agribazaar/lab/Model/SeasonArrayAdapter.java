package com.agribazaar.lab.Model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.agribazaar.lab.R;

public class SeasonArrayAdapter extends ArrayAdapter<Season> {

    private ArrayList<Season> seasonArrayList;

    public SeasonArrayAdapter(@NonNull Context context, int textViewResourceId, @NonNull ArrayList<Season> countryList) {
        super(context, 0, textViewResourceId, countryList);

        seasonArrayList = new ArrayList<>(countryList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return countryFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.country_autocomplete_row,parent,false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.text_view_name);


        Season seasonItem = getItem(position);

        textViewName.setText(seasonItem.getSeason_name());

        return convertView;
    }

    public Season getSelectedItem(int position){
        return seasonArrayList.get(position);
    }

    private Filter countryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            FilterResults results = new FilterResults();

            List<Season> suggestions = new ArrayList<>();

            if(charSequence == null || charSequence.length() == 0){

                suggestions.addAll(seasonArrayList);

            } else {

                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (Season item : seasonArrayList) {

                    if(item.getSeason_name().toLowerCase().contains(charSequence)){

                        suggestions.add(item);

                    }
                }

            }

            results.values = suggestions;

            results.count =suggestions.size();

            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            clear();

            addAll((List)filterResults.values);

            notifyDataSetChanged();

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {

            return ((Season) resultValue).getSeason_name();
        }
    };
}
