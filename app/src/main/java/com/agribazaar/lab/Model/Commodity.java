package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Commodity implements Parcelable {
    private String com_id;
    private String com_name;

    public Commodity(String i, String com_name) {
        this.com_id = i;
        this.com_name = com_name;
    }


    public String getCom_id() {
        return com_id;
    }

    public void setCom_id(String com_id) {
        this.com_id = com_id;
    }

    public String getCom_name() {
        return com_name;
    }

    public void setCom_name(String com_name) {
        this.com_name = com_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.com_id);
        dest.writeString(this.com_name);
    }

    public Commodity() {
    }

    protected Commodity(Parcel in) {
        this.com_id = in.readString();
        this.com_name = in.readString();
    }

    public static final Parcelable.Creator<Commodity> CREATOR = new Parcelable.Creator<Commodity>() {
        @Override
        public Commodity createFromParcel(Parcel source) {
            return new Commodity(source);
        }

        @Override
        public Commodity[] newArray(int size) {
            return new Commodity[size];
        }
    };
}
