package com.agribazaar.lab.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CommodityDetailModel implements Parcelable {
    private String season;
    private String commodity;
    private String parameter;
    private String com_image_path;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCom_image_path() {
        return com_image_path;
    }

    public void setCom_image_path(String com_image_path) {
        this.com_image_path = com_image_path;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public CommodityDetailModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.season);
        dest.writeString(this.commodity);
        dest.writeString(this.parameter);
        dest.writeString(this.com_image_path);
        dest.writeInt(this.id);
    }

    protected CommodityDetailModel(Parcel in) {
        this.season = in.readString();
        this.commodity = in.readString();
        this.parameter = in.readString();
        this.com_image_path = in.readString();
        this.id = in.readInt();
    }

    public static final Creator<CommodityDetailModel> CREATOR = new Creator<CommodityDetailModel>() {
        @Override
        public CommodityDetailModel createFromParcel(Parcel source) {
            return new CommodityDetailModel(source);
        }

        @Override
        public CommodityDetailModel[] newArray(int size) {
            return new CommodityDetailModel[size];
        }
    };
}
