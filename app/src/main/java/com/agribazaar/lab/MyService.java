package com.agribazaar.lab;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.View;

import com.agribazaar.lab.Model.CommodityImageResponse;
import com.agribazaar.lab.Model.CommodityModel;
import com.agribazaar.lab.Network.NetworkClient;
import com.agribazaar.lab.Network.UploadApis;
import com.agribazaar.lab.network.HttpService;
import com.agribazaar.lab.network.NetworkCallBackListener;
import com.agribazaar.lab.network.ProgressiveRequestBody;
import com.agribazaar.lab.network.ResponseListener;
import com.agribazaar.lab.options.Commons;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.agribazaar.lab.Model.CommodityDetailModel;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyService extends IntentService {

    private ArrayList<CommodityDetailModel> commodityPostModelListDb = new ArrayList<>();

    public MyService(String name) {
        super(name);
    }

    public MyService(){
        super("my_intent_thread");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Log.e("tag", "isService Running  first--" + ComodityImageApplication.isServiceRunning);
        if (!ComodityImageApplication.isServiceRunning) {
            ComodityImageApplication.isServiceRunning = true;
            Log.e("tag", "isService Running--" + ComodityImageApplication.isServiceRunning);
            Log.e("tag","status-----"+ConnectivityUtils.isNetworkConnected(getApplicationContext()));
            if (ConnectivityUtils.isNetworkConnected(getApplicationContext())) {      // Start Uploading Documents
                DataBaseHandler dataBaseHandler = new DataBaseHandler(MyService.this);
                commodityPostModelListDb = dataBaseHandler.getCommodityData();
                Log.e("tag", "getch All Entries in DB :  " + commodityPostModelListDb.size());
                startProcess();
            } else {
//                clearNotification();
                ComodityImageApplication.isServiceRunning = false;
                Log.e("tag", "Network not connected");
//                FL.d("GTSService", "No Internet onHandleIntent");
            }

        }
    }

    private void startProcess() {
        Log.e("tag", "startProcess() called...................................");

        if (commodityPostModelListDb != null && !commodityPostModelListDb.isEmpty()) {
            CommodityDetailModel commodityDetailModel = commodityPostModelListDb.get(0);
            commodityPostModelListDb.remove(0);
            startUploadingProcess(commodityDetailModel);
        }else{
            ComodityImageApplication.isServiceRunning = false;
//            clearNotification();
        }
    }

    private void startUploadingProcess(final CommodityDetailModel commodityDetailModel) {
        Log.e("tag", "startUploadingPocess() called...................................");
            if (commodityDetailModel.getCom_image_path() != null) {    // means this gts have server id

/*                ArrayList<GTSAttachmentModel> needToUploadToS3List = DataBaseHelper.getInstance().getNeedToUploadList(gtsPostModel.getLocalGTSID());
                if(StarNovaApplication.gtsAttachmentModelList.isEmpty()){
                    StarNovaApplication.gtsAttachmentModelList=needToUploadToS3List;
                }else {
                    StarNovaApplication.gtsAttachmentModelList.addAll(needToUploadToS3List);
                }*/
                uploadCheck(commodityDetailModel);
/*                if ( StarNovaApplication.gtsAttachmentModelList.size() > 0) {
                    uploadCheck(commodityDetailModel);
                } else {
                    postAttachments(commodityDetailModel);
                }*/

            } else {
                commodityPostModelListDb.remove(0);
                startUploadingProcess(commodityDetailModel);
            }
        }


    private void uploadCheck(CommodityDetailModel commodityDetailModel) {

        Log.e("tag", "uploadCheck() called...................................");
        String imagePath = commodityDetailModel.getCom_image_path();
        Log.e("tag", "uploadCheck() called..................................."+imagePath);
        String imagePath1 = commodityDetailModel.getCom_image_path().replace("file://","/storage/emulated/0/0/dev/CameraApp");
        File file = new File(imagePath1);
        Log.e("tag", "New  Image path..................................."+file.getPath());
//        uploadToS3(file, commodityDetailModel);
        uploadAttachment(file, commodityDetailModel);

/*
        if (StarNovaApplication.gtsAttachmentModelList.size()>0) {
            GTSAttachmentModel gtsAttachmentModel = StarNovaApplication.gtsAttachmentModelList.get(0);
            StarNovaApplication.gtsAttachmentModelList.remove(0);
            if (gtsAttachmentModel != null && Utils.isValidText(gtsAttachmentModel.getLocal_path())) {
                String imagePath = gtsAttachmentModel.getLocal_path().replace("file://", "");
                File file = new File(imagePath);
                if (!DataBaseHelper.getInstance().isUploaded(gtsAttachmentModel.getAttachment_id()) && file.exists()) {
                    uploadToS3(file, commodityDetailModel, gtsAttachmentModel);
                } else {
                    uploadCheck(commodityDetailModel);
                }
            } else {
                uploadCheck(commodityDetailModel);
            }
        } else {
            startUploadingProcess();
        }*/
    }

    public void uploadAttachment(final File file, final CommodityDetailModel commodityDetailModel) {
        String url = "http://103.104.73.27:5002/api/v1/image_upload";
//        showProgress();
        final HashMap<String, String> data = new HashMap<>();
        data.put("commodity", commodityDetailModel.getCommodity());
        data.put("category", commodityDetailModel.getParameter());
//        showNotification();
        uploadProgressiveFile(url, file.getAbsolutePath(), data, new NetworkCallBackListener(new ResponseListener() {
            @Override
            public void onSuccess(int request_id, Object responseBody) {
//                hideProgress();
                if (responseBody instanceof String) {
                    final CommodityImageResponse dataResponse = Util.getModel(responseBody.toString(), CommodityImageResponse.class);
                    if (dataResponse != null && !dataResponse.isError()) {
                        if (dataResponse.getData() != null) {
                            File fdelete = new File(commodityDetailModel.getCom_image_path());
                            if (fdelete.exists()) {
                                if (fdelete.delete()) {
                                    Logger.e("file Deleted :" + commodityDetailModel.getCom_image_path());
                                } else {
                                    Logger.e("file not Deleted :" + commodityDetailModel.getCom_image_path());
                                }
                            }

                            Logger.e("data upload success====="+dataResponse.getData().getImage_path()+"-----"+commodityDetailModel.getId());
                            DataBaseHandler dataBaseHandler = new DataBaseHandler(MyService.this);
                            dataBaseHandler.deleteCommodityImage(commodityDetailModel.getId());
//                            startProcess();

                            uploadCheck(commodityDetailModel);
                        }/* else {
                            Logger.i("tag", "Single Commodity Image upload failed");
                            ComodityImageApplication.isServiceRunning = false;
                            Logger.i("tag", "isService Running" + ComodityImageApplication.isServiceRunning);
                            startProcess();

                        }*/

                    }

                }
            }

            @Override
            public void onError(int request_id, String error_message) {
//                hideProgress();
//                new SweetAlertDialog(AgribazaarCommodityTesting.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Error").setContentText(error_message).show();

            }

            @Override
            public void onRequestTimeout() {
//                hideProgress();
//                new SweetAlertDialog(AgribazaarCommodityTesting.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Something went wrong").show();

            }
        }), new ProgressiveRequestBody.Listener() {
            @Override
            public void onRequestProgress(long bytesWritten, long contentLength) {
//                MyService.this.updateProgressMessage("Uploading... " + (int) (100 * bytesWritten / contentLength) + "%");
            }
        });
    }

    public void uploadProgressiveFile(String url, String fileUri, HashMap<String, String> dataMap, NetworkCallBackListener callback, ProgressiveRequestBody.Listener progressListener) {

/*        if (Util.isValidText(fileUri)) {
            if (fileUri.startsWith("file://")) {
                fileUri = fileUri.substring(7, fileUri.length());
            }
        }
        File file = FileUtils.getFile(this, fileUri);

        if (file == null || !file.exists()) {
            hideProgress();
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Attachment does not exist").show();
            return;
        }*/
        File file = new File(fileUri);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        ProgressiveRequestBody fileBody = new ProgressiveRequestBody(requestFile, progressListener);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), fileBody);
        Logger.e("url-------"+url);
        Logger.e("dataMap-------"+dataMap.toString());
        Logger.e("filepart-------"+filePart);
        Logger.e("file-------"+file.getAbsolutePath());
//        HttpService.getInstance().upload(url, dataMap, filePart, callback);
        try {
            HttpService.getInstance().upload(url, dataMap, filePart, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.i("tag", "On Destroy called.................................");
    }


    /*
    private void uploadToS3(final File file, final CommodityDetailModel commodityDetailModel) {
        Log.e("tag", "uploadGTSCopyS3() called...................................");
        Log.e("tag", "Image file..................................."+file.getAbsolutePath());

        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        Log.e("extention :  ", extension);
        CognitoCachingCredentialsProvider credentialsProvider =
                new CognitoCachingCredentialsProvider(
                        MyService.this.getApplicationContext(),
                        "ap-south-1:9b422687-43da-484e-ace3-3ff3e985ef66",
                        Regions.AP_SOUTH_1);

        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);
        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(MyService.this)
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        long currentTime = System.currentTimeMillis();
        final String randomImageName = "IMAGE" + currentTime + extension;
        Log.i("imagename", randomImageName);
        final TransferObserver observer = transferUtility.upload(Constants.STARNOVA_S3_BUCKET, Constants.STARNOVA_S3_BUCKET_GTS_FOLDER + randomImageName, file, CannedAccessControlList.PublicRead); // The location of the file to be uploaded )

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
//                    Log.e("tag", "attachment uploaded id "+gtsAttachmentModel.getAttachment_id());
                    String imageUrl = "https://" + Constants.STARNOVA_S3_BUCKET + ".s3.amazonaws.com/" + Constants.STARNOVA_S3_BUCKET_GTS_FOLDER + randomImageName;
                    Log.e("tag", imageUrl);
//                    DataBaseHelper.getInstance().updateGTSAttachment(gtsAttachmentModel.getAttachment_id(), imageUrl);
//                    uploadCheck(gtsPostModel);
                }
                if (state.name().equals("FAILED")) {
                    Log.e("tag", "Single GTScopy S3 upload failed");
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("tag", "Error");
            }

        });
    }
*/

/*
    private void uploadImageToServer (final File file, CommodityDetailModel commodityDetailModel) {
        Retrofit retrofit = NetworkClient.getRetrofitClient(this);
        UploadApis uploadAPIs = retrofit.create(UploadApis.class);

        //Create a file object using file path
//        File file = new File("two.jpg");

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // Create MultipartBody.Part using file request-body,file name and part name
//        MultipartBody.Part part = MultipartBody.Part.createFormData("uploadImage", file.getName(), fileReqBody);
        MultipartBody.Part part = MultipartBody.Part.createFormData("uploadImage", file.getName(), fileReqBody);

        //Create request body with text description and text media type
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");

        Call call = uploadAPIs.uploadImage(part, description);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                Log.e("tag", "Api call success---"+response.message());

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("tag", "Api call fail");
            }
        });




    }
*/


/*
    private void postAttachments(final CommodityDetailModel commodityDetailModel) {

        Log.e("tag", "postAttachments() called...................................");

        String gtsCopyUrl = "";
        String managerPicUrl = "";
        String stackStencilPicUrl = "";


        GTSAttachmentModel gtsCopyAttachment = DataBaseHelper.getInstance().getSingleAttachments(gtsPostModel.getLocalGTSID(), Constants.GTS_COPY);
        if (gtsCopyAttachment != null) {
            gtsCopyUrl = gtsCopyAttachment.getServer_path();
        }



        final String url = Constants.HOST_URL_HTTP + "gtsMobileApi.php";
        final Map<String, Object> data = new HashMap<>();
        data.put("cis_no_id", gtsPostModel.getGts_server_id());
        data.put("gtsCopy", gtsCopyUrl);
        data.put("ManagerPicwithStack", managerPicUrl);
        data.put("StencilPhotograph", stackStencilPicUrl);
        data.put("physicalCopy", physicalCopyurl);

        HttpService.getInstance().putStringData(url, data, new NetworkCallBackListener(new ResponseListener() {
            @Override
            public void onSuccess(int request_id, Object responseBody) {
                if (responseBody instanceof String) {
                    final GTSModelResponse dataResponse = Utils.getModel(responseBody.toString(), GTSModelResponse.class);
                    if (dataResponse != null) {
                        if (!dataResponse.isError()) {
                            Logger.i("tag ", "post successfully Attachments ");
                            DataBaseHelper.getInstance().updatePhysicalCopyStatus(gtsPostModel.getLocalGTSID());
                            getVehicles(gtsPostModel, gtsPostModel.getVehicle_stack_details());

                        }
                    }
                }
            }

            @Override
            public void onError(int request_id, String error_message) {
            }

            @Override
            public void onRequestTimeout() {
            }
        }

        ));
    }
*/



}
