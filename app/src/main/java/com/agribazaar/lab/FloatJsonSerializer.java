package com.agribazaar.lab;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public class FloatJsonSerializer implements JsonSerializer<Float> {
    @Override
    public JsonElement serialize(final Float src, final Type typeOfSrc, final JsonSerializationContext context) {
        BigDecimal bigValue = BigDecimal.valueOf(src);

        return new JsonPrimitive(bigValue.toPlainString());
    }
}
