package com.agribazaar.lab.options;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.agribazaar.lab.R;

public class ImageShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_show);
        ImageView myimageview = findViewById(R.id.image);
        String image_path = getIntent().getStringExtra("picture");
        Bitmap bitmap = BitmapFactory.decodeFile(image_path);
        myimageview.setImageBitmap(bitmap);
    }
}
