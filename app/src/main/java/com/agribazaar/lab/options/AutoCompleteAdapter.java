package com.agribazaar.lab.options;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import com.agribazaar.lab.Model.Commodity;
import com.agribazaar.lab.R;

public class AutoCompleteAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Commodity> originalList;
    private ArrayList<Commodity> suggestions = new ArrayList<>();
    private Filter filter = new CustomFilter();

    public AutoCompleteAdapter(ArrayList<Commodity> originalList) {
        this.originalList = originalList;
        this.suggestions=originalList;
    }

    @Override
    public int getCount() {
        return suggestions.size(); // Return the size of the suggestions list.
    }

    @Override
    public Object getItem(int position) {
        return suggestions.get(position).getCom_name();
    }

    public Commodity getSelectedItem(int position){
        return suggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
/*        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.autoText.setText(suggestions.get(position).getCom_name());

        return convertView;*/
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) parent.getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.support_simple_spinner_dropdown_item, null);
        }
        Commodity product = suggestions.get(position);
        if (product != null) {
            TextView productLabel = (TextView)  v.findViewById(android.R.id.text1);
            if (productLabel != null) {
                productLabel.setText(product.getCom_name());
            }
        }
        return v;
    }

    public void setOriginalList(ArrayList<Commodity> newList) {
        originalList = newList;
        suggestions.clear();
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
    }

    /**
     * Our Custom Filter Class.
     */
    private class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Commodity> suggestionsList = new ArrayList<>();
            if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < originalList.size(); i++) {
                    if (originalList.get(i).getCom_name().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                        suggestionsList.add(originalList.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestionsList;
            results.count = suggestionsList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if(results.values!=null) {
                suggestions = (ArrayList<Commodity>) results.values;
            }else {
                suggestions=originalList;
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
