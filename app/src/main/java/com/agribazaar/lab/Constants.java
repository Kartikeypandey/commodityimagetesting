package com.agribazaar.lab;

public class Constants {
    public static final String STARNOVA_S3_BUCKET = "starnova";
    public static final String STARNOVA_S3_ML_BUCKET = "agribazaar-ml/";
    public static final String STARNOVA_S3_BUCKET_GTS_FOLDER = "gts/";

    public static final String ACCESS_TOKEN = "Authorization";
    public static final String PLATFORM = "Platform";
    public static final String DATA_TOKEN = "token";
    public static String HOST_URL = "http://apistg.agribazaar.com/";
    public static final String USERS_TYPE = "User-Type";
    public static final String VERSION = "version";
    public static final int NOTIFICATION_ID_UPLOADING = 204;




}
