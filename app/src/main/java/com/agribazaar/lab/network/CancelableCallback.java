//package com.agribazaar.lab.network;
//
//import com.agribazaar.android.core.ActivityLifecycle;
//import com.agribazaar.android.core.BaseActivity;
//import com.agribazaar.android.utils.ConnectivityUtils;
//import com.agribazaar.android.utils.Logger;
//
//import java.net.ConnectException;
//import java.net.SocketTimeoutException;
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
// * Created by bashiruddin on 8/5/17.
// */
//
//public abstract class CancelableCallback<T> implements Callback<T> {
//    private static List<CancelableCallback> mList = new ArrayList<>();
//
//    private boolean isCanceled = false;
//    private Object mTag = null;
//    private boolean showError = true;
//    private int requestId;
//
//    public static void cancelAll() {
//        Iterator<CancelableCallback> iterator = mList.iterator();
//        while (iterator.hasNext()) {
//            iterator.next().isCanceled = true;
//            iterator.remove();
//        }
//    }
//
//    public static void cancel(Object tag) {
//        if (tag != null) {
//            Iterator<CancelableCallback> iterator = mList.iterator();
//            CancelableCallback item;
//            while (iterator.hasNext()) {
//                item = iterator.next();
//                if (tag.equals(item.mTag)) {
//                    item.isCanceled = true;
//                    iterator.remove();
//                }
//            }
//        }
//    }
//
//    public CancelableCallback() {
//        mList.add(this);
//    }
//
//    public CancelableCallback(Object tag) {
//        mTag = tag;
//        mList.add(this);
//    }
//
//    public CancelableCallback(int requestId) {
//        mList.add(this);
//        this.requestId = requestId;
//    }
//
//    public CancelableCallback(int requestId, Object tag) {
//        mTag = tag;
//        mList.add(this);
//        this.requestId = requestId;
//    }
//
//    public CancelableCallback(boolean showError) {
//        mList.add(this);
//        this.showError = showError;
//    }
//
//    public CancelableCallback(Object tag, boolean showError) {
//        mTag = tag;
//        mList.add(this);
//        this.showError = showError;
//    }
//
//    public CancelableCallback(int requestId, boolean showError) {
//        mList.add(this);
//        this.showError = showError;
//        this.requestId = requestId;
//    }
//
//    public CancelableCallback(int requestId, Object tag, boolean showError) {
//        mTag = tag;
//        mList.add(this);
//        this.showError = showError;
//        this.requestId = requestId;
//    }
//
//    public void cancel() {
//        isCanceled = true;
//        mList.remove(this);
//    }
//
//
//    @Override
//    public final void onFailure(Call<T> call, Throwable t) {
//        mList.remove(this);
//        if (!isCanceled) {
//            if (t instanceof ConnectException || t instanceof UnknownHostException) {
//                if (ConnectivityUtils.getConnectivityStatus() == ConnectivityUtils.NETWORK_STATUS_NOT_CONNECTED) {
//                    onError(requestId, "No Internet Connection");
//                    if (showError && ActivityLifecycle.getInstance().getActivity() != null) {
//                        if (ActivityLifecycle.getInstance().getActivity() instanceof BaseActivity) {
//                            ((BaseActivity) ActivityLifecycle.getInstance().getActivity()).showNoInternetConnection();
//                        }
//                    }
//                } else {
//                    onError(requestId, "Could not connect to server");
//                }
//            } else if (t instanceof SocketTimeoutException) {
//                onRequestTimeout(requestId);
//            } else {
//                onError(requestId, "Something went wrong");
//            }
//        } else {
//            Logger.e("Request cancelled by user");
//        }
//    }
//
//    @Override
//    public void onResponse(Call<T> call, Response<T> response) {
//        if (!isCanceled) {
//            if (response.isSuccessful()) {
//                onSuccess(requestId, response.body());
//            } else {
//                onError(requestId, "Something went wrong");
//            }
//        } else {
//            Logger.e("Request cancelled by user");
//        }
//        mList.remove(this);
//
//    }
//
//    public abstract void onSuccess(int id, T response);
//
//    public abstract void onError(int id, String error_message);
//
//    public abstract void onRequestTimeout(int id);
//
//}
