package com.agribazaar.lab.network;

/**
 * Created by Bashir on 6/6/2016.
 */

public interface ResponseListener<T> {
    void onSuccess(int request_id, Object responseBody);
    void onError(int request_id, String error_message);
//    void onConnectionError(int request_id);
    void onRequestTimeout();
}
