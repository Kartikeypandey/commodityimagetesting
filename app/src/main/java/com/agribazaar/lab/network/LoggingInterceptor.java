package com.agribazaar.lab.network;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class LoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long t1 = System.nanoTime();
        String requestLog = String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers());
        if (request.method().compareToIgnoreCase("post") == 0) {
            requestLog = "\n" + requestLog + "\n" + bodyToString(request);
        } else if (request.method().compareToIgnoreCase("put") == 0) {
            requestLog = "\n" + requestLog + "\n" + bodyToString(request);
        }

        Response response = chain.proceed(request);
        long t2 = System.nanoTime();

        String responseLog = String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers());

        String bodyString = response.body().string();


        if (request.method().compareToIgnoreCase("post") == 0 || request.method().compareToIgnoreCase("put") == 0) {
//            FL.d(Utils.getStringToBase64(String.format("Sending request %s on %s%n%s",
//                    request.url(), chain.connection(), request.headers())));
//            FL.d("request" + "\n" + Utils.getStringToBase64(requestLog));
//            FL.d( "response" + "\n" + Utils.getStringToBase64(responseLog + "\n" + bodyString));
        }
        return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
    }


    public static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}