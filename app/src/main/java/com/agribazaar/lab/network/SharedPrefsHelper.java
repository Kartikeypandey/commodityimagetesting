package com.agribazaar.lab.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import java.util.Map;


public class SharedPrefsHelper {
    private static final String SHARED_PREFS_NAME = "dalmiamedicare";

    private static SharedPrefsHelper instance;

    private SharedPreferences sharedPreferences;

    public static synchronized SharedPrefsHelper getInstance() {
        if (instance == null) {
            instance = new SharedPrefsHelper();
        }

        return instance;
    }

//    private SharedPrefsHelper() {
//        instance = this;
//        sharedPreferences = AgriBazaarApplication.getInstance().getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
//    }

    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            getEditor().remove(key).apply();
        }
    }

    public void save(String key, Object value) {
        SharedPreferences.Editor editor = getEditor();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value instanceof Uri) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.apply();
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = getEditor();
        editor.remove(key).apply();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) sharedPreferences.getAll().get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public int getInt(String key, int defValue){
        return sharedPreferences.getInt(key,defValue);
    }
    public boolean has(String key) {
        return sharedPreferences.contains(key);
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public void clearAll() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

//    public void clearAllPreferences() {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        SharedPreferences preferences = sharedPreferences;
//        Map<String, ?> prefs = preferences.getAll();
//        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
//            if (AgriBazaarApplication.getInstance().getTuteList().contains(prefToReset.getKey())) {
//                continue;
//            }
//            editor.remove(prefToReset.getKey());
//        }
//        editor.apply();
//    }
}
