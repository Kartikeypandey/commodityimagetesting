package com.agribazaar.lab.network;

import android.content.Context;

import com.agribazaar.lab.BuildConfig;
import com.agribazaar.lab.Constants;
import com.agribazaar.lab.DoubleJsonSerializer;
import com.agribazaar.lab.FloatJsonSerializer;
import com.agribazaar.lab.SharedPreferencesUtil;
import com.agribazaar.lab.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by Bashir on 17-Jun-16.
 */
public class HttpService {

    private static HttpService instance = new HttpService();
    private RestService restService, restServiceNew;
    private okhttp3.OkHttpClient okHttpClient, okHttpClientNew;
    public String ACCESS_TOKEN;

    private HttpService() {
    }

    public static HttpService getInstance() {
        /*if (!ConnectivityUtils.isNetworkConnected()) {
//            new SweetAlertDialog(AgriBazaarApplication.getInstance(),SweetAlertDialog.ERROR_TYPE).setTitleText("No Internet").setContentText("Please check internet connection").show();
        }*/
        return instance;
    }

    public void setUp(Context context) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        okhttp3.Cache cache = new okhttp3.Cache(context.getCacheDir(), 1024);

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        okhttp3.OkHttpClient.Builder okHttpClientBuilder = new okhttp3.OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).hostnameVerifier(getHostnameVerifier())
                /*.addInterceptor(new UpLoadProgressInterceptor(new ProgressiveRequestBody.Listener() {
                    @Override
                    public void onRequestProgress(long bytesWritten, long contentLength) {
                        Logger.e("Data "+ bytesWritten+" Total "+contentLength);
                    }
                }))*/.cache(cache);
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(logging);
            okHttpClientBuilder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS));
        }
        okHttpClientBuilder.addInterceptor(new LoggingInterceptor());

//        final String access_token = SharedPreferencesUtil.getValue(Constants.DATA_TOKEN, "");
//        if (Util.isValidText(access_token)) {
//            okHttpClientBuilder.addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request request = chain.request().newBuilder().addHeader(Constants.ACCESS_TOKEN, access_token).addHeader("User-Agent", "Android").addHeader(Constants.PLATFORM, "Android").addHeader(Constants.USERS_TYPE, "1").addHeader(Constants.VERSION, BuildConfig.VERSION_NAME).build();
//                    return chain.proceed(request);
//                }
//            });
//        } else if (Util.isValidText(ACCESS_TOKEN)) {
//            okHttpClientBuilder.addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request request = chain.request().newBuilder().addHeader(Constants.ACCESS_TOKEN, ACCESS_TOKEN).addHeader("User-Agent", "Android").addHeader(Constants.PLATFORM, "Android").addHeader(Constants.USERS_TYPE, "1").addHeader(Constants.VERSION, BuildConfig.VERSION_NAME).build();
//                    return chain.proceed(request);
//                }
//            });
//        } else {
//            okHttpClientBuilder.addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request request = chain.request().newBuilder().addHeader("User-Agent", "Android").addHeader(Constants.PLATFORM, "Android").addHeader(Constants.USERS_TYPE, "1").addHeader(Constants.VERSION, BuildConfig.VERSION_NAME).build();
//                    return chain.proceed(request);
//                }
//            });
//        }

        okHttpClient = okHttpClientBuilder.build();
        GsonBuilder builder = new GsonBuilder();
//        builder.registerTypeAdapter(FacetPivot.class, new LocationFacetDeserializer());
        builder.registerTypeAdapter(Double.class, new DoubleJsonSerializer());
        builder.registerTypeAdapter(Float.class, new FloatJsonSerializer());
//        builder.setLongSerializationPolicy( LongSerializationPolicy.STRING );
        Gson gson = builder.setLenient().setPrettyPrinting().create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.HOST_URL)
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build();

        restService = retrofit.create(RestService.class);
        setUpNoHeader(context);
    }

    public void setUpNoHeader(Context context) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        okhttp3.Cache cache = new okhttp3.Cache(context.getCacheDir(), 1024);

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        okhttp3.OkHttpClient.Builder okHttpClientBuilder = new okhttp3.OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).hostnameVerifier(getHostnameVerifier())
                /*.addInterceptor(new UpLoadProgressInterceptor(new ProgressiveRequestBody.Listener() {
                    @Override
                    public void onRequestProgress(long bytesWritten, long contentLength) {
                        Logger.e("Data "+ bytesWritten+" Total "+contentLength);
                    }
                }))*/.cache(cache);
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(logging);
            okHttpClientBuilder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS));
        }
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("User-Agent", "Android").addHeader(Constants.PLATFORM, "Android").addHeader(Constants.USERS_TYPE, "1").addHeader(Constants.VERSION, BuildConfig.VERSION_NAME).build();
                return chain.proceed(request);
            }
        });
        okHttpClientBuilder.addInterceptor(new LoggingInterceptor());

       /* final String access_token = SharedPreferencesUtil.getValue(Constants.DATA_TOKEN, "");
        if (Utils.isValidText(access_token)) {
            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().addHeader(Constants.ACCESS_TOKEN, access_token).build();
                    return chain.proceed(request);
                }
            });
        } else if (Utils.isValidText(ACCESS_TOKEN)) {
            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().addHeader(Constants.ACCESS_TOKEN, ACCESS_TOKEN).build();
                    return chain.proceed(request);
                }
            });
        }*/

        okHttpClientNew = okHttpClientBuilder.build();
        GsonBuilder builder = new GsonBuilder();
//        builder.registerTypeAdapter(FacetPivot.class, new LocationFacetDeserializer());
        builder.registerTypeAdapter(Double.class, new DoubleJsonSerializer());
        builder.registerTypeAdapter(Float.class, new FloatJsonSerializer());
//        builder.setLongSerializationPolicy( LongSerializationPolicy.STRING );
        Gson gson = builder.setLenient().setPrettyPrinting().create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.HOST_URL)
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClientNew).build();

        restServiceNew = retrofit.create(RestService.class);
    }

    public void cancelAllRequests() {
        try {
            if (okHttpClient != null) {
                okHttpClient.dispatcher().cancelAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*    public void getJsonResponse(String url, NetworkCallBackListener callback) {
        Call<JsonResponse> call = restService.getJsonResponse(url);
        call.enqueue(callback);
    }*/

    private static HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true; // verify always returns true, which could cause insecure network traffic due to trusting TLS/SSL server certificates for wrong hostnames
                //HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                //return hv.verify("localhost", session);
            }
        };
    }
/*
    public void userLogin(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<UserSignUp> call = restService.userLogin(url, userData);
        call.enqueue(callback);
    }

    public void postOTP(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<OTPLoginData> call = restService.postOTP(url, userData);
        call.enqueue(callback);
    }*/


/*
    public void updateStage(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<UserSignUp> call = restService.updateStage(url, userData);
        call.enqueue(callback);
    }

    public void getCommodities(String url, NetworkCallBackListener callback) {
        Call<CommodityResponse> call = restService.getCommodities(url);
        call.enqueue(callback);
    }

    public void getAlliances(String url, NetworkCallBackListener callback) {
        Call<AllianceResponse> call = restService.getAlliances(url);
        call.enqueue(callback);
    }

    public void getSellersList(String url, NetworkCallBackListener callback) {
        Call<SellerJsonResponse> call = restService.getSellersList(url);
        call.enqueue(callback);
    }

    public void getBuyersList(String url, NetworkCallBackListener callback) {
        Call<BuyerJsonResponse> call = restService.getBuyersList(url);
        call.enqueue(callback);
    }

    public void getBags(String url, NetworkCallBackListener callback) {
        Call<BagResponse> call = restService.getBags(url);
        call.enqueue(callback);
    }

    public void validateWrSr(String url, NetworkCallBackListener callback) {
        Call<WareHouseResponse> call = restService.validateWrSr(url);
        call.enqueue(callback);
    }

    public void getAddress(String url, NetworkCallBackListener callback) {
        Call<AddressResponse> call = restService.getAddress(url);
        call.enqueue(callback);
    }

    public void getMandies(String url, NetworkCallBackListener callback) {
        Call<MandiResponse> call = restService.getMandies(url);
        call.enqueue(callback);
    }

    public void getCommodityParams(String url, NetworkCallBackListener callback) {
        Call<CommodityParamList> call = restService.getCommodityParams(url);
        call.enqueue(callback);
    }

    public void getRequestType(String url, NetworkCallBackListener callback) {
        Call<RequestTypeResponse> call = restService.getRequestType(url);
        call.enqueue(callback);
    }

    public void getJsonObject(String url, NetworkCallBackListener callback) {
        Call<String> call = restService.getJsonObject(url);
        call.enqueue(callback);
    }

    public void getMarketPrice(String url, NetworkCallBackListener callback) {
        Call<MarketPriceResponse> call = restService.getMarketPrice(url);
        call.enqueue(callback);
    }

    public void getSellCostDetails(String url, NetworkCallBackListener callback) {
        Call<SellDetailResponse> call = restService.getSellCostDetails(url);
        call.enqueue(callback);
    }

    public void getPaymentBreakup(String url, NetworkCallBackListener callback) {
        Call<PaymentDataResponse> call = restService.getPaymentBreakup(url);
        call.enqueue(callback);
    }

    public void getSellOrderDetails(String url, NetworkCallBackListener callback) {
        Call<SellOrderDetailResponse> call = restService.getSellOrderDetails(url);
        call.enqueue(callback);
    }

    public void getBuyOrderDetails(String url, NetworkCallBackListener callback) {
        Call<BuyOrderDetailResponse> call = restService.getBuyOrderDetails(url);
        call.enqueue(callback);
    }

    public void postData(String url, HashMap<String, Object> data, NetworkCallBackListener callback) {
        Call<DataResponse> call = restService.postData(url, data);
        call.enqueue(callback);
    }

    public void putData(String url, HashMap<String, Object> data, NetworkCallBackListener callback) {
        Call<DataResponse> call = restService.putData(url, data);
        call.enqueue(callback);
    }

    public void getData(String url, NetworkCallBackListener callback) {
        Call<DataResponse> call = restService.getData(url);
        call.enqueue(callback);
    }

    public void getAccountInfo(String url, NetworkCallBackListener callback) {
        Call<AccountInfoResponse> call = restService.getAccountInfo(url);
        call.enqueue(callback);
    }

    public void getAuctions(String url, NetworkCallBackListener callback) {
        Call<NafedAuctionTypeResponseModel> call = restService.getAuctions(url);
        call.enqueue(callback);
    }

    public void getFunds(String url, NetworkCallBackListener callback) {
        Call<FundResponse> call = restService.getFunds(url);
        call.enqueue(callback);
    }


    public void getMyStock(String url, NetworkCallBackListener callback) {
        Call<MyStockResponse> call = restService.getMyStock(url);
        call.enqueue(callback);
    }

    public void getStockDetails(String url, NetworkCallBackListener callback) {
        Call<StockDetailResponse> call = restService.getStockDetails(url);
        call.enqueue(callback);
    }

    public void getNews(String url, NetworkCallBackListener callback) {
        Call<NewsResponse> call = restService.getNews(url);
        call.enqueue(callback);
    }

    public void postOneSignalID(String url, Map<String, Object> data, NetworkCallBackListener callback) {
        Call<UserSignUp> call = restService.postOneSignalID(url, data);
        call.enqueue(callback);
    }

    public void postFunds(String url, Map<String, Object> data, NetworkCallBackListener callback) {
        Call<FundResponse> call = restService.postFunds(url, data);
        call.enqueue(callback);
    }

    public void updateOneSignalID(String url, Map<String, Object> data, NetworkCallBackListener callback) {
        Call<DataResponse> call = restService.updateOneSignalID(url, data);
        call.enqueue(callback);
    }

    public void getPlatformOrders(String url, NetworkCallBackListener callback) {
        Call<PlatformOrderResponse> call = restService.getPlatformOrders(url);
        call.enqueue(callback);
    }

    public void getGraphData(String url, NetworkCallBackListener callback) {
        Call<GraphResponse> call = restService.getGraphData(url);
        call.enqueue(callback);
    }

    public void getDefaultDoc(String url, NetworkCallBackListener callback) {
        Call<DefaultDataDocResponse> call = restService.getDefaultDoc(url);
        call.enqueue(callback);
    }


    public void getSellOrderResponse(String url, NetworkCallBackListener callback) {
        Call<SellOrderResponse> call = restService.getSellOrderResponse(url);
        call.enqueue(callback);
    }

    public void getBuyOrderResponse(String url, NetworkCallBackListener callback) {
        Call<BuyOrderResponse> call = restService.getBuyOrderResponse(url);
        call.enqueue(callback);
    }

    public void getMyAuctions(String url, NetworkCallBackListener callback) {
        Call<MyAuctionsResponse> call = restService.getMyAuctions(url);
        call.enqueue(callback);
    }

    public void getChatData(String url, NetworkCallBackListener callback) {
        Call<ChatDataResponse> call = restService.getChatData(url);
        call.enqueue(callback);
    }

    public Call<ResponseBody> downloadFile(String url) {
        return restService.downloadFile(url);
    }

    public Call<ResponseBody> downloadS3File(String url) {
        return restServiceNew.downloadS3File(url);
    }

    public void getAccountInfo(String url, CancelableCallback callback) {
        Call<AccountInfoResponse> call = restService.getAccountInfo(url);
        call.enqueue(callback);
    }

    public void getNafedAuctions(String url, NetworkCallBackListener callback) {
        Call<String> call = restService.getNafedAuctions(url);
        call.enqueue(callback);
    }

    public Call<UserSignUp> upload(String url, RequestBody requestBodyHolderName, RequestBody requestBodyAccNo, RequestBody requestBodyBankName, RequestBody requestBodyIfsc, MultipartBody.Part part) {
        Call<UserSignUp> call = restService.upload(url, requestBodyHolderName, requestBodyAccNo, requestBodyBankName, requestBodyIfsc, part);
        return call;
    }

    public Call<String> upload(String url, Map<String, String> dataMap, MultipartBody.Part file) {
        Call<String> call = restService.upload(url, dataMap, file);
        return call;
    }
*/
    public void upload(String url, Map<String, String> dataMap, MultipartBody.Part file, NetworkCallBackListener callback) {
        Call<String> call = restService.upload(url, dataMap, file);
        call.enqueue(callback);
    }

/*
    public void postFirm(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<ProfileFirmModel> call = restService.postFirm(url, userData);
        call.enqueue(callback);
    }

    public void getFirm(String url, NetworkCallBackListener callback) {
        Call<ProfileFirmModel> call = restService.getFirm(url);
        call.enqueue(callback);
    }

    public void putFirm(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<ProfileFirmModel> call = restService.putFirm(url, userData);
        call.enqueue(callback);
    }

    public void deleteFirm(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<RequestTypeResponse> call = restService.deleteFirm(url, userData);
        call.enqueue(callback);
    }

    public void postDirector(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<ProfileFirmDirectorModel> call = restService.postDirector(url, userData);
        call.enqueue(callback);
    }

    public void getDirector(String url, NetworkCallBackListener callback) {
        Call<ProfileFirmDirectorModel> call = restService.getDirector(url);
        call.enqueue(callback);
    }

    public void putDirector(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<ProfileFirmDirectorModel> call = restService.putDirector(url, userData);
        call.enqueue(callback);
    }

    public void deleteDirector(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<RequestTypeResponse> call = restService.deleteDirector(url, userData);
        call.enqueue(callback);
    }

    public void postGSTCredentials(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<RequestTypeResponse> call = restService.postGSTCredentials(url, userData);
        call.enqueue(callback);
    }


    public void getStringData(String url, NetworkCallBackListener callback) {
        Call<String> call = restService.getStringData(url);
        call.enqueue(callback);
    }

    public void postStringData(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<String> call = restService.postStringData(url, userData);
        call.enqueue(callback);
    }

    public void postString(String url, String userData, NetworkCallBackListener callback) {
        Call<String> call = restService.postString(url, userData);
        call.enqueue(callback);
    }

    public void putStringData(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<String> call = restService.putStringData(url, userData);
        call.enqueue(callback);
    }

    public void putString(String url, String userData, NetworkCallBackListener callback) {
        Call<String> call = restService.putString(url, userData);
        call.enqueue(callback);
    }

    public void deleteStringData(String url, Map<String, Object> userData, NetworkCallBackListener callback) {
        Call<String> call = restService.deleteStringData(url, userData);
        call.enqueue(callback);
    }
*/

    /*public Observable<String> getStarredRepositories(String url) {
       return restService.getStarredRepositories(url);
    }*/
}
