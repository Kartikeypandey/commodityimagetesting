package com.agribazaar.lab.network;



import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

/**
 * Created by Bashir on 17-Jun-16.
 */
interface RestService {
//    @GET
//    Call<JsonResponse> getJsonResponse(@Url String url);
//
//    @GET
//    Call<DefaultDataDocResponse> getDefaultDoc(@Url String url);
//
//    @POST
//    Call<UserSignUp> userLogin(@Url String url, @Body Map<String, Object> data);
//
//    @POST
//    Call<OTPLoginData> postOTP(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<CommodityResponse> getCommodities(@Url String url);
//
//    @GET
//    Call<AllianceResponse> getAlliances(@Url String url);
//
//    @GET
//    Call<SellerJsonResponse> getSellersList(@Url String url);
//
//
//    @GET
//    Call<BuyerJsonResponse> getBuyersList(@Url String url);
//
//    @GET
//    Call<BagResponse> getBags(@Url String url);
//
//    @GET
//    Call<WareHouseResponse> validateWrSr(@Url String url);
//
//    @GET
//    Call<AddressResponse> getAddress(@Url String url);
//
//    @GET
//    Call<MandiResponse> getMandies(@Url String url);
//
//    @GET
//    Call<CommodityParamList> getCommodityParams(@Url String url);
//
//    @GET
//    Call<RequestTypeResponse> getRequestType(@Url String url);
//
//    @GET
//    Call<String> getJsonObject(@Url String url);
//
//    @GET
//    Call<MarketPriceResponse> getMarketPrice(@Url String url);
//
//    @GET
//    Call<SellDetailResponse> getSellCostDetails(@Url String url);
//
//    @GET
//    Call<PaymentDataResponse> getPaymentBreakup(@Url String url);
//
//    @GET
//    Call<SellOrderDetailResponse> getSellOrderDetails(@Url String url);
//
//    @GET
//    Call<BuyOrderDetailResponse> getBuyOrderDetails(@Url String url);
//
//    @POST
//    Call<DataResponse> postData(@Url String url, @Body Map<String, Object> data);
//
//    @PUT
//    Call<DataResponse> putData(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<DataResponse> getData(@Url String url);
//
//    @GET
//    Call<AccountInfoResponse> getAccountInfo(@Url String url);
//
//    @GET
//    Call<NafedAuctionTypeResponseModel> getAuctions(@Url String url);
//
//    @GET
//    Call<FundResponse> getFunds(@Url String url);
//
//    @GET
//    Call<MyStockResponse> getMyStock(@Url String url);
//
//    @GET
//    Call<StockDetailResponse> getStockDetails(@Url String url);
//
//    @PUT
//    Call<UserSignUp> updateStage(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<NewsResponse> getNews(@Url String url);
//
//    @POST
//    Call<UserSignUp> postOneSignalID(@Url String url, @Body Map<String, Object> data);
//
//    @POST
//    Call<FundResponse> postFunds(@Url String url, @Body Map<String, Object> data);
//
//    @PUT
//    Call<DataResponse> updateOneSignalID(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<PlatformOrderResponse> getPlatformOrders(@Url String url);
//
//    @GET
//    Call<GraphResponse> getGraphData(@Url String url);
//
//
//    @GET
//    Call<SellOrderResponse> getSellOrderResponse(@Url String url);
//
//
//    @GET
//    Call<BuyOrderResponse> getBuyOrderResponse(@Url String url);
//
//
//    @GET
//    Call<MyAuctionsResponse> getMyAuctions(@Url String url);
//
//    @GET
//    Call<ChatDataResponse> getChatData(@Url String url);
//
//    @GET
//    @Streaming
//    Call<ResponseBody> downloadFile(@Url String url);
//
//    @GET
//    @Streaming
//    Call<ResponseBody> downloadS3File(@Url String url);
//
//    @GET
//    Call<String> getNafedAuctions(@Url String url);
//
//    @Multipart
//    @POST
//    Call<UserSignUp> upload(@Url String url, @Part("file") RequestBody name, @Part MultipartBody.Part file, @Part("data") Map<String, Object> data);
//
//    @Multipart
//    @POST
//    Call<UserSignUp> upload(@Url String url, @Part("name_in_bank") RequestBody name_in_bank, @Part("acc_no") RequestBody acc_no, @Part("bank_name") RequestBody bank_name, @Part("ifsc") RequestBody ifsc, @Part MultipartBody.Part file);
//
    @Multipart
    @POST
    Call<String> upload(@Url String url, @PartMap Map<String, String> params, @Part MultipartBody.Part file);
//
//    @POST
//    Call<ProfileFirmModel> postFirm(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<ProfileFirmModel> getFirm(@Url String url);
//
//
//    @PUT
//    Call<ProfileFirmModel> putFirm(@Url String url, @Body Map<String, Object> data);
//
//    @HTTP(method = "DELETE", hasBody = true)
//    Call<RequestTypeResponse> deleteFirm(@Url String url, @Body Map<String, Object> data);
//
//    @POST
//    Call<ProfileFirmDirectorModel> postDirector(@Url String url, @Body Map<String, Object> data);
//
//    @GET
//    Call<ProfileFirmDirectorModel> getDirector(@Url String url);
//
//    @PUT
//    Call<ProfileFirmDirectorModel> putDirector(@Url String url, @Body Map<String, Object> data);
//
//    @HTTP(method = "DELETE", hasBody = true)
//    Call<RequestTypeResponse> deleteDirector(@Url String url, @Body Map<String, Object> data);
//
//
//
//    @POST
//    Call<RequestTypeResponse> postGSTCredentials(@Url String url, @Body Map<String, Object> data);


    @GET
    Call<String> getStringData(@Url String url);

    @PUT
    Call<String> putStringData(@Url String url, @Body Map<String, Object> data);

    @PUT
    Call<String> putString(@Url String url, @Body String data);

    @HTTP(method = "DELETE", hasBody = true)
    Call<String> deleteStringData(@Url String url, @Body Map<String, Object> data);

    @POST
    Call<String> postStringData(@Url String url, @Body Map<String, Object> data);

    @POST
    Call<String> postString(@Url String url, @Body String data);


    /*@GET
    Observable<String> getStarredRepositories(@Url String url);*/

//    @HTTP(method = "DELETE", hasBody = true)
//    Call<DataResponse> updateOneSignalID(@Url String url, @Body Map<String, Object> data);
}
