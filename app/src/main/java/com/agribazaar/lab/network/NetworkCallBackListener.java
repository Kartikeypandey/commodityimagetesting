package com.agribazaar.lab.network;


import android.content.DialogInterface;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;


import com.agribazaar.lab.ActivityLifecycle;
import com.agribazaar.lab.ConnectivityUtils;
import com.agribazaar.lab.Constants;
import com.agribazaar.lab.MainActivity;
import com.agribazaar.lab.SharedPreferencesUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Bashir on 6/6/2016.
 */

public class NetworkCallBackListener<T> implements Callback<T> {
    private ResponseListener mResponseListener;
    private int requestId;
    private boolean showError = true;
    private long mLastClickTime = 0;

    public NetworkCallBackListener(int request_id, ResponseListener listener) {
        this.mResponseListener = listener;
        this.requestId = request_id;
    }

    public NetworkCallBackListener(ResponseListener listener) {
        this.mResponseListener = listener;
    }

    public NetworkCallBackListener(ResponseListener listener, boolean showError) {
        this.mResponseListener = listener;
        this.showError = showError;
    }

    /*public NetworkCallBackListener(ResponseListener<ResponseBody> listener) {
        this.mResponseListener = listener;
    }*/

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (ActivityLifecycle.getInstance().getActivity() != null && ActivityLifecycle.getInstance().getActivity() instanceof MainActivity) {
//            ((MainActivity) ActivityLifecycle.getInstance().getActivity()).hideNoInternetConnection();
        }
        if (mResponseListener != null) {
            if (response.isSuccessful()) {
                mResponseListener.onSuccess(requestId, response.body());
            } else if (response.code() == 511) {
                if (SharedPreferencesUtil.has(Constants.DATA_TOKEN)) {
                    SharedPreferencesUtil.delete(Constants.DATA_TOKEN);
                    SharedPreferencesUtil.clearAll();
/*                    if (ActivityLifecycle.getInstance().getActivity() != null) {
                        HttpService.getInstance().setUp(ActivityLifecycle.getInstance().getActivity());
//                        AgriBazaarApplication.getInstance().createShortcuts();
                        new SweetAlertDialog(ActivityLifecycle.getInstance().getActivity(), SweetAlertDialog.WARNING_TYPE).setTitleText("Session Expired").setContentText("Please login again.").setDisMissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
//                                ((MainActivity) ActivityLifecycle.getInstance().getActivity()).showLogin();
                            }
                        }).show();
                    }*/
                }
            } else {
                mResponseListener.onError(requestId, "Something went wrong");
            }
        }
    }

    @Override
    public void onFailure(@NonNull final Call<T> call, @NonNull Throwable t) {
        if (mResponseListener != null) {
            if (t instanceof ConnectException || t instanceof UnknownHostException) {
                if (ConnectivityUtils.getConnectivityStatus() == ConnectivityUtils.NETWORK_STATUS_NOT_CONNECTED) {
                    mResponseListener.onError(requestId, "No Internet Connection");
                    if (ActivityLifecycle.getInstance().getActivity() != null) {
                        if (ActivityLifecycle.getInstance().getActivity() instanceof MainActivity) {
//                            ((MainActivity) ActivityLifecycle.getInstance().getActivity()).showNoInternetConnection();
//                            ((MainActivity) ActivityLifecycle.getInstance().getActivity()).setRetryClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    try {
//                                        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.CLICK_TIME) {
//                                            Logger.e("********** Click Discarded **********");
//                                            return;
//                                        }
//                                        mLastClickTime = SystemClock.elapsedRealtime();
//                                        if(call.isExecuted()) {
//                                            call.clone().enqueue(NetworkCallBackListener.this);
//                                        } else {
//                                            call.enqueue(NetworkCallBackListener.this);
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            });
                        }
//                        new SweetAlertDialog(ActivityLifecycle.getInstance().getActivity(), SweetAlertDialog.ERROR_TYPE).setTitleText("No Internet Connection").setContentText("Please check your connection and try again!").show();
                    }
//                    mResponseListener.onConnectionError(requestId);
                } else {
                    mResponseListener.onError(requestId, "Could not connect to server");
                }
            } else if (t instanceof SocketTimeoutException) {
//                mResponseListener.onError(requestId, /*t.getMessage()*/"request timeout");
//                if (count == 0) {
                mResponseListener.onRequestTimeout();
//                } else {
//                    mResponseListener.onError(requestId, /*t.getMessage()*/"Something went wrong");
//                }
//                count++;
            } else {
                mResponseListener.onError(requestId, "Something went wrong");
            }
        }
    }
}
