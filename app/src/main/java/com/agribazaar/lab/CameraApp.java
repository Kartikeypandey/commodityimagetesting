package com.agribazaar.lab;

import android.app.Application;
import android.util.Log;

public class CameraApp extends Application {
    private static CameraApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("Application","Main Context");

    }

    public static synchronized CameraApp getInstance() {
        return instance;
    }

}
