package com.agribazaar.lab;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.agribazaar.lab.Model.CommodityDetailModel;
import com.agribazaar.lab.Receiver.NetworkSchedulerService;
import com.agribazaar.lab.network.HttpService;

import java.io.File;
import java.util.ArrayList;

public class ComodityImageApplication extends Application {
    private static ComodityImageApplication instance;
//    private Tracker mTracker;
    public static ArrayList<CommodityDetailModel> gtsAttachmentModelList = new ArrayList<>();
    public static boolean isServiceRunning = false;



/*
    static {
        if (BuildConfig.DEBUG) {
            if (Constants.IS_STAGING) {
                Constants.HOST_URL_HTTPS = "http://apistg.agribazaar.com/";
                Constants.HOST_URL_HTTP = "http://novastg.agribazaar.com/";
                Constants.HOST_NAFED_PRODUCTION_URL_HTTP = "http://nafedstg.agribazaar.com/admin/webp/";
                Constants.HOST_PRODUCTION_AGRIBAZAAR_URL_HTTP = "http://apistg.agribazaar.com/";
                Constants.UID_PRE_TEXT = "abc_stg_";
            } else if (Constants.IS_UAT) {
                Constants.HOST_URL_HTTPS = "http://apiuat.agribazaar.com/";
                Constants.HOST_URL_HTTP = "http://novauat.agribazaar.com/";
                Constants.HOST_NAFED_PRODUCTION_URL_HTTP = "http://nafeduat.agribazaar.com/admin/webp/";
                Constants.HOST_PRODUCTION_AGRIBAZAAR_URL_HTTP = "http://apiuat.agribazaar.com/";
                Constants.UID_PRE_TEXT = "abc_uat_";
                Constants.CRF_HOST_URL = "http://crfuat.agribazaar.com/v2/";
                Constants.CRF_BUCKET = "crfuat";
                Constants.FASTRACK_BUCKET="startrack-dev";
            } else if (Constants.IS_PRODUCTION) {
                Constants.HOST_URL_HTTPS = "https://api.agribazaar.com/";
                Constants.HOST_URL_HTTP = "https://www.agrigates.com/";
                Constants.HOST_NAFED_PRODUCTION_URL_HTTP = "https://nafed.agribazaar.com/admin/webp/";
                Constants.HOST_PRODUCTION_AGRIBAZAAR_URL_HTTP = "https://api.agribazaar.com/";
                Constants.UID_PRE_TEXT = "abc_";
                Constants.CRF_HOST_URL = "https://www.agrigates.com/v2/";
                Constants.CRF_BUCKET = "crfprod";
                Constants.FASTRACK_BUCKET="startrack";
            } else {
                Constants.HOST_URL_HTTPS = "http://apidemo.agribazaar.com/";
                Constants.HOST_URL_HTTP = "https://novademo.agribazaar.com/";
                Constants.HOST_NAFED_PRODUCTION_URL_HTTP = "http://nafeddemo.agribazaar.com/admin/webp/";
                Constants.HOST_PRODUCTION_AGRIBAZAAR_URL_HTTP = "http://apidemo.agribazaar.com/";
                Constants.UID_PRE_TEXT = "abc_demo_";
                Constants.CRF_HOST_URL = "https://crfdemo.agribazaar.com/v2/";
                Constants.FASTRACK_BUCKET="startrack-dev";
            }
        } else {
            Constants.HOST_URL_HTTPS = "https://api.agribazaar.com/";
            Constants.HOST_URL_HTTP = "https://www.agrigates.com/";
            Constants.HOST_NAFED_PRODUCTION_URL_HTTP = "https://nafed.agribazaar.com/admin/webp/";
            Constants.HOST_PRODUCTION_AGRIBAZAAR_URL_HTTP = "https://api.agribazaar.com/";
            Constants.UID_PRE_TEXT = "abc_";
            Constants.CRF_HOST_URL = "https://www.agrigates.com/v2/";
            Constants.CRF_BUCKET = "crfprod";
            Constants.FASTRACK_BUCKET="startrack";
        }
    }
*/

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        ActivityLifecycle.init(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        instance = this;

        Log.e("Application","Main Context");
/*
        upgradeSecurityProvider();
*/
        HttpService.getInstance().setUp(getApplicationContext());
//        setupPicasso();
//        setLogger();
//        setUpOneSignal();


/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob();
        }*/
    }

    public static synchronized ComodityImageApplication getInstance() {
        return instance;
    }


/*
    private void setupPicasso() {
        File customCacheDirectory = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/.StarNova");
        if (!customCacheDirectory.exists()) {
            customCacheDirectory.mkdirs();
        }
        okhttp3.Cache cache = new okhttp3.Cache(*/
/*getCacheDir()*//*
customCacheDirectory, Integer.MAX_VALUE);
        okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient.Builder().cache(cache).build();
        OkHttp3Downloader downloader = new OkHttp3Downloader(okHttpClient);
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(downloader);
        builder.addRequestHandler(new VideoRequestHandler());
        Picasso built = builder.build();
    */
/*    if (BuildConfig.DEBUG) {
            built.setIndicatorsEnabled(true);
            built.setLoggingEnabled(true);
        }*//*


        Picasso.setSingletonInstance(built);
    }
*/

/*
    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Logger.i("**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
        SharedPreferencesUtil.clear();
    }
*/

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

/*
    synchronized public Tracker getGATracker() {
        if (mTracker == null) {
            // Setting mTracker to Analytics Tracker declared in our xml Folder
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.analytics_tracker);
        }
        return mTracker;
    }
*/


//    private void setLogger() {
//        File customCacheDirectory = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/StarNova");
//        FL.init(new FLConfig.Builder(this)/*.logger(...)*/       // customise how to hook up with logcat
//                /*.defaultTag("DEBUG")*/   // customise default tag
//                /*.minLevel(FLConst.Level.V)*/   // customise minimum logging level
//                .logToFile(true)   // enable logging to file
//                .dir(customCacheDirectory)    // customise directory to hold log files
//                /*.formatter(...)*/    // customise log format and file name
//                .retentionPolicy(FLConst.RetentionPolicy.FILE_COUNT) // customise retention strategy
//                .maxFileCount(FLConst.DEFAULT_MAX_FILE_COUNT)    // customise how many log files to keep if retention by file count
//                .maxTotalSize(FLConst.DEFAULT_MAX_TOTAL_SIZE)    // customise how much space log files can occupy if retention by total size
//                .build());
//        FL.setEnabled(true);
//    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class)).setRequiresCharging(true).setMinimumLatency(1000).setOverrideDeadline(2000).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setPersisted(true).build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }

/*
    public void setUpOneSignal() {
        OneSignal.startInit(this).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.*/
/*None*//*
Notification).setNotificationReceivedHandler(new MyNotificationReceivedHandler()).setNotificationOpenedHandler(new MyNotificationOpenedHandler()).init();
        if (SharedPreferencesUtil.getValue(Constants.ONE_SIGNAL_ID, "").isEmpty()) {
            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                @Override
                public void idsAvailable(String userId, String registrationId) {
                    Logger.d("User:" + userId);
                    SharedPreferencesUtil.save(Constants.ONE_SIGNAL_ID, userId);
                    if (registrationId != null) Logger.d("registrationId:" + registrationId);
//                    registerUser(userId);
                }
            });
        } else {
            Logger.e("User:" + SharedPreferencesUtil.getValue(Constants.ONE_SIGNAL_ID, ""));
            Logger.e("User Reg:" + SharedPreferencesUtil.getValue(Constants.REGISTERED_ONE_SIGNAL_ID, ""));

            if (!SharedPreferencesUtil.getValue(Constants.ONE_SIGNAL_ID, "").equals(SharedPreferencesUtil.getValue(Constants.REGISTERED_ONE_SIGNAL_ID, ""))) {
//                registerUser(SharedPreferencesUtil.getValue(Constants.ONE_SIGNAL_ID, ""));
            }
        }
    }
*/


/*
    private void upgradeSecurityProvider() {
        final int ERROR_DIALOG_REQUEST_CODE = 1;
        ProviderInstaller.installIfNeededAsync(this, new ProviderInstaller.ProviderInstallListener() {
            @Override
            public void onProviderInstalled() {
                Logger.e("Providers installed ");
            }

            @Override
            public void onProviderInstallFailed(int errorCode, Intent recoveryIntent) {
                GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
                if (availability.isUserResolvableError(errorCode)) {
                    // Recoverable error. Show a dialog prompting the user to
                    // install/update/enable Google Play services.
                    availability.showErrorDialogFragment(ActivityLifecycle.getInstance().getActivity(),
                            errorCode,
                            ERROR_DIALOG_REQUEST_CODE,
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    // The user chose not to take the recovery action
//                                    onProviderInstallerNotAvailable();
                                }
                            });
                } else {
                    // Google Play services is not available.
//                    onProviderInstallerNotAvailable();
                }
                Logger.e("Providers installation Failed ");
            }
        });
    }
*/
}
