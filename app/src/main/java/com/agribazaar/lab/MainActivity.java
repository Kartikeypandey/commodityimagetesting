package com.agribazaar.lab;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agribazaar.lab.Model.CommodityModel;
import com.agribazaar.lab.Model.ParametersModel;
import com.agribazaar.lab.Network.NetworkClient;
import com.agribazaar.lab.Network.UploadApis;
import com.bumptech.glide.Glide;
import com.agribazaar.lab.Model.CommodityDetailModel;
import com.agribazaar.lab.options.Commons;
import com.google.gson.GsonBuilder;
import com.jakewharton.rxbinding2.view.RxView;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;

import com.agribazaar.lab.Model.Commodity;

import com.agribazaar.lab.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

    private View prepareToRecord;
    private static final int PERMISSION_REQUEST_CODE = 200;


    @BindView(R.id.media_dir) TextView mediaDir;
    @BindView(R.id.gallery) GridView gallery;
    private List<File> mediaFiles = new ArrayList<>();
    private MediaFileAdapter adapter;

    private AutoCompleteTextView acCommodity, acVariety, acSeason;
    private Commodity /*commodityModel*/varietyModel;
    private CommodityModel commodityModel;
    private ParametersModel parametersModel;

    private TextInputLayout tilCommodity, tilVariety, tilSeason;
    public Handler progressHandler;
    private String randomImageName;
    private String imageUrl;
    private Button btnUploadImage;
    private ArrayList<CommodityModel> commodityModelArrayList = new ArrayList<>();
    private ArrayList<ParametersModel> parametersModelArrayList = new ArrayList<>();
    private ArrayList<ParametersModel> parametersModelArrayList1 = new ArrayList<>();

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        acCommodity = findViewById(R.id.ac_commodity);
        tilCommodity = findViewById(R.id.til_commodity);
        acVariety = findViewById(R.id.ac_variety);
        tilVariety = findViewById(R.id.til_variety);
/*        acSeason = findViewById(R.id.ac_season);
        tilSeason = findViewById(R.id.til_season);*/
        btnUploadImage = findViewById(R.id.btnUpload);

        if (ContextCompat.checkSelfPermission(MainActivity.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)  {
            Toast.makeText(MainActivity.this, "You have already granted permission",Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }

        try {
            readJsonData();
        } catch (JSONException e) {
            e.printStackTrace();
        }


            acCommodity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCommodity.setError(null);
                commodityModel = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        acVariety.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilVariety.setError(null);
                varietyModel = null;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



//        AutoCompleteArrayAdapter autoCompleteAdapter = new AutoCompleteArrayAdapter(MainActivity.this,0,commodityList);
        AutoCompleteArrayAdapter autoCompleteAdapter = new AutoCompleteArrayAdapter(MainActivity.this,0,commodityModelArrayList);
        acCommodity.setThreshold(1);

        acCommodity.setAdapter(autoCompleteAdapter);
        acCommodity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                commodityModel = autoCompleteAdapter.getItem(position);
                Log.e("commAdapter","value"+commodityModel.getComName());
                acVariety.setText("");
                parametersModelArrayList1.addAll(parametersModelArrayList);
//                parameterArrayAdapter.notifyDataSetChanged();
            }
        });



      ParameterAutoCompleteArrayAdapter parameterAutoCompleteArrayAdapter = new ParameterAutoCompleteArrayAdapter(MainActivity.this,0,parametersModelArrayList);
        acVariety.setAdapter(parameterAutoCompleteArrayAdapter);
        acVariety.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parametersModel = parameterAutoCompleteArrayAdapter.getItem(position);
                Log.e("varAdapter","value"+parametersModel.getName());
            }
        });





        RxPermissions rxPermissions = new RxPermissions(this);
        Button btn = (Button)findViewById(R.id.open_camera);
//        prepareToRecord = findViewById(R.id.open_camera);
        RxView.clicks(btn)
                .compose(rxPermissions.ensure(CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE))
                .subscribe(granted -> {
                    if (granted) {
                        if (commodityModel == null) {
                            tilCommodity.setError("Select commodity");
                        }else if (parametersModel == null){
                            tilVariety.setError("Select parameter");
                        } else {
                            Log.e("MainActivity","value"+commodityModel.getComName()+"-----"+parametersModel.getName());
                            startVideoRecordActivity();
                        }
                    } else {
                        Snackbar.make(btn, getString(R.string.no_enough_permission), Snackbar.LENGTH_SHORT).setAction("Confirm", null).show();
                    }
                });

//        mediaDir.setText(String.format("Media files are saved under:\n%s", Commons.MEDIA_DIR));
        mediaDir.setText(String.format("Your Image files:"));
/*        if (Commons.MEDIA_DIR != null) {
            mediaDir.setText(String.format("Captured Image:\n%s", Commons.MEDIA_DIR));
        } else {
            mediaDir.setText("");

        }*/


        adapter = new MediaFileAdapter(this, mediaFiles);
        gallery.setAdapter(adapter);
        gallery.setOnItemClickListener((parent, view, position, id) -> {
            File file = adapter.getItem(position);
            Log.e("MainActivity","FILe"+file.getAbsolutePath());
            playOrViewMedia(file);
        });

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIntentServiceToUploadImage();
            }
        });

        if (ComodityImageApplication.isServiceRunning) {
            if (ConnectivityUtils.isNetworkConnected(getApplicationContext())) {      // Start Uploading Documents
                startIntentServiceToUploadImage();
            }
        }

    }


    private void startVideoRecordActivity() {
        Intent intent = new Intent(this, PhotographerActivity.class);
        intent.putExtra("commodity", commodityModel.getComName());
        intent.putExtra("parameter",parametersModel.getName());
        startActivity(intent);
//        startActivityForResult(intent,101);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        Logger.e("onResume Called");

        if (ContextCompat.checkSelfPermission(MainActivity.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)  {
            File file = new File(Commons.MEDIA_DIR);
            Log.e("TAG","File length"+file.length()+"----Root Path---"+file.getAbsolutePath()+"----"+file.isDirectory());
            mediaFiles.clear();
            File[] listFile = file.listFiles();
            Logger.e("length--"+file.listFiles().length);

            RecursivePrint(listFile, 0, 0);
        }
//        Log.e("TAG","FileListed---------"+file.isFile());

/*
        if (file.isDirectory()) {
//            mediaFiles.clear();
            File[] files = file.listFiles();
            // new code
*/
/*
            for (File inFile : files) {
                if (inFile.isDirectory()) {
                    // is directory
//                    Log.e("GrandDirectory","subdir1"+inFile.getAbsolutePath());
                    File[] f1 = inFile.listFiles();

                    for (File inFile1 : f1) {
//                        Log.e("Count","GrandDirectory"+f1.length);
                        if (inFile1.isDirectory()) {
                            // is directory
                            Log.e("ParentDirectory","subdir2"+inFile1.getAbsolutePath());

                            File[] f2 = inFile1.listFiles();
                            for (File inFile2 : f2) {
                                if (inFile2.isDirectory()) {

                                    // is directory
                                    Log.e("ChildDirectory","subdir3"+inFile2.getAbsolutePath());
//                                    mediaFiles.clear();
                                    File [] f3 = inFile2.listFiles();
                                    mediaFiles.clear();
                                    Log.e("mainActivity","f3 length----"+f3.length);
                                    for (File inFile3 : f3) {
                                        if (inFile3.isDirectory()) {
                                            // is directory
                                            Log.e("SubChildDirectory","subdir4>>>>>>>>>>>"+inFile3.getAbsolutePath());

                                        } else {
//                                            mediaFiles.clear();
                                            Log.e("SubChildDirectory","subdir4-------------"+inFile3.getAbsolutePath());
                                            if (f3 != null) {
                                                Arrays.sort(f3, (f9, f10) -> {
                                                    if (f9.lastModified() - f10.lastModified() == 0) {
                                                        return 0;
                                                    } else {
                                                        return f9.lastModified() - f10.lastModified() > 0 ? -1 : 1;
                                                    }
                                                });
                                                mediaFiles.addAll(Arrays.asList(f3));
                                                adapter.notifyDataSetChanged();
                                            } *//*

*/
/*else {
                                                adapter.notifyDataSetChanged();
                                            }*//*
*/
/*

                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
*//*

*/
/*            //
            if (files != null) {
                Arrays.sort(files, (f1, f2) -> {
                    if (f1.lastModified() - f2.lastModified() == 0) {
                        return 0;
                    } else {
                        return f1.lastModified() - f2.lastModified() > 0 ? -1 : 1;
                    }
                });
                mediaFiles.addAll(Arrays.asList(files));
                adapter.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetChanged();
            }*//*

*/
/*            Arrays.sort(files, (f1, f2) -> {
                if (f1.lastModified() - f2.lastModified() == 0) {
                    return 0;
                } else {
                    return f1.lastModified() - f2.lastModified() > 0 ? -1 : 1;
                }
            });
            mediaFiles.addAll(Arrays.asList(files));
            adapter.notifyDataSetChanged();*//*

        }
*/
    }

    private void playOrViewMedia(File file) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uriForFile = FileProvider.getUriForFile(MainActivity.this, getApplicationContext().getPackageName() + ".provider", file);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            uriForFile = Uri.fromFile(file);
        }
        intent.setDataAndType(uriForFile, isVideo(file) ? "video/mp4" : "image/png");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;

        if (isIntentSafe) {
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, "No media viewer found", Toast.LENGTH_SHORT).show();
        }

    }

    public class MediaFileAdapter extends BaseAdapter {

        private List<File> files;

        private Context context;

        MediaFileAdapter(Context c, List<File> files) {
            context = c;
            this.files = files;
        }


        public int getCount() {
            return files != null ? files.size() : 0;
        }

        public File getItem(int position) {
            return files.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.item_media, parent, false);
            }
            imageView = convertView.findViewById(R.id.item_image);
            View indicator = convertView.findViewById(R.id.item_indicator);
            File file = getItem(position);
            if (isVideo(file)) {
                indicator.setVisibility(View.VISIBLE);
            } else {
                indicator.setVisibility(View.GONE);
            }
            Glide.with(context).load(file).into(imageView);
            return convertView;
        }
    }

    private boolean isVideo(File file) {
        return file != null && file.getName().endsWith(".mp4");
    }


    private void startIntentServiceToUploadImage() {
        Intent serviceIntent = new Intent(MainActivity.this, MyService.class);
        startService(serviceIntent);
    }


    void RecursivePrint(File[] arr,int index,int level)
    {
        // terminate condition
        if(index == arr.length)
            return;

        // tabs for internal levels
        for (int i = 0; i < level; i++)
            System.out.print("\t");

        // for files
        if(arr[index].isFile()) {
            System.out.println(arr[index].getAbsolutePath());
            mediaFiles.add(new File(arr[index].getAbsolutePath()));
            adapter.notifyDataSetChanged();

            // for sub-directories

        } else if(arr[index].isDirectory())
        {
//            System.out.println("[" + arr[index].getName() + "]");

            // recursion for sub-directories
            RecursivePrint(arr[index].listFiles(), 0, level + 1);
        }

        // recursion for main directory
        RecursivePrint(arr,++index, level);
    }


    private void readJsonData () throws JSONException {
        InputStream is = getResources().openRawResource(R.raw.commodityparams);
        Scanner sc = new Scanner(is);
        StringBuilder stringBuilder = new StringBuilder();
        while (sc.hasNextLine()) {
            stringBuilder.append(sc.nextLine());
        }
//        parseJson(stringBuilder.toString());
        CommodityModel commodityModel = Util.getModel(stringBuilder.toString(), CommodityModel.class);
        commodityModelArrayList.add(commodityModel);
//        Log.e("Model", "comID------"+commodityModel.getComId());
//        Log.e("Model", "comName-----"+commodityModel.getComName());
//        Log.e("Model", "parameters------");
        parametersModelArrayList.addAll(commodityModel.getParameters());

        for (int i =0 ; i <commodityModel.getParameters().size();i++) {
            ParametersModel parametersModel = new ParametersModel();
            parametersModel.setId(commodityModel.getParameters().get(i).getId());
            parametersModel.setName(commodityModel.getParameters().get(i).getName());
//            Log.e("param", "id----"+parametersModel.getId());
//            Log.e("param", "name----"+parametersModel.getName());

        }

    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE)
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this,"Permission Granted",Toast.LENGTH_LONG).show();
                }

    }


}
