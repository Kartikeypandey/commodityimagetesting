package com.agribazaar.lab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import com.agribazaar.lab.Model.CommodityDetailModel;

public class DataBaseHandler {

    private static final String TABLE_NAME = "commodity_image_table";
    private static final String DATABASE_NAME = "mydatabae";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_COMMODITY_IMAGE_TABLE = "CREATE TABLE commodity_image_table(id INTEGER PRIMARY KEY AUTOINCREMENT,season TEXT DEFAULT NULL,com_name TEXT DEFAULT NULL,parameter TEXT DEFAULT NULL,com_image_path TEXT DEFAULT NULL,clock_stamp TIMESTAMP DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')))";

    DatabaseHelper databaseHelper;
    Context context;
    SQLiteDatabase sqLiteDatabase;

    public DataBaseHandler(Context context) {
        this.context = context;
        databaseHelper = new DatabaseHelper(context);
    }


    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME,null,DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_COMMODITY_IMAGE_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS mytable");
            onCreate(db);
        }
    }

    public DataBaseHandler open () {
        sqLiteDatabase = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close () {
        databaseHelper.close();
    }

    public long insertCommodityImage (CommodityDetailModel commodityDetailModel) {
        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        long id = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("com_name", commodityDetailModel.getCommodity());
            contentValues.put("parameter", commodityDetailModel.getParameter());
            contentValues.put("com_image_path", commodityDetailModel.getCom_image_path());
            id = db.insert(TABLE_NAME, null, contentValues);
            Logger.e("ID " + id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return id;
    }


    public long insertData (CommodityDetailModel commodityDetailModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("season",commodityDetailModel.getSeason());
        contentValues.put("com_name", commodityDetailModel.getCommodity());
        contentValues.put("parameter", commodityDetailModel.getParameter());
        contentValues.put("com_image_path", commodityDetailModel.getCom_image_path());
        return sqLiteDatabase.insertOrThrow(TABLE_NAME, null, contentValues);
    }

    public ArrayList<CommodityDetailModel> getCommodityData () {
        ArrayList<CommodityDetailModel> commodityPostList = new ArrayList<>();
        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        String getQuery = "select * from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(getQuery,null);
        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        int id = cursor.getInt(cursor.getColumnIndex("id"));
                        String season = cursor.getString(cursor.getColumnIndex("season"));
                        String comName = cursor.getString(cursor.getColumnIndex("com_name"));
                        String parameter = cursor.getString(cursor.getColumnIndex("parameter"));
                        String comImagePath = cursor.getString(cursor.getColumnIndex("com_image_path"));
                        String clockStamp = cursor.getString(cursor.getColumnIndex("clock_stamp"));

                        CommodityDetailModel commodityDetailModel = new CommodityDetailModel();
                        commodityDetailModel.setId(id);
                        commodityDetailModel.setSeason(season);
                        commodityDetailModel.setCommodity(comName);
                        commodityDetailModel.setParameter(parameter);
                        commodityDetailModel.setCom_image_path(comImagePath);

                        commodityPostList.add(commodityDetailModel);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return commodityPostList;
    }

    public long updateCommodityImage(long attachmentId, String imageUrl) {
        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        long id = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("season", imageUrl);
//            contentValues.put("upload_status", 1);
//            id = db.update(GTS_ATTACHMENTS, contentValues, "attachment_id = ?", new String[]{String.valueOf(attachmentId)});
            id = db.update(TABLE_NAME, contentValues, "season = ?", new String[]{imageUrl});
            Logger.i("tag", "commodity_image_table update db" + id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return id;
    }

    public long deleteCommodityImage(long vehicleId) {
        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        long id = 0;
        try {
            db.delete(TABLE_NAME, "id = ?", new String[]{String.valueOf(vehicleId)});

            Logger.i("tag", "delete vehicle " + vehicleId);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return id;
    }



}
