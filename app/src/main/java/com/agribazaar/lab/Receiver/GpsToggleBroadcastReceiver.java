package com.agribazaar.lab.Receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.agribazaar.lab.ConnectivityUtils;
import com.agribazaar.lab.Logger;
import com.agribazaar.lab.MyService;


/**
 * Created by Bashir on 30-Aug-16.
 */

public class GpsToggleBroadcastReceiver extends BroadcastReceiver {
    int i = 0;

/*    private ConnectivityReceiverListener mConnectivityReceiverListener;

    public GpsToggleBroadcastReceiver(ConnectivityReceiverListener listener) {
        mConnectivityReceiverListener = listener;
    }*/

    public GpsToggleBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("com.staragri.app.location.enabled"));
            }
        } else if (intent.getAction().matches("android.net.conn.CONNECTIVITY_CHANGE") || intent.getAction().matches("android.net.wifi.WIFI_STATE_CHANGED")) {
            int status = ConnectivityUtils.getConnectivityStatusString();
            if (status != ConnectivityUtils.NETWORK_STATUS_NOT_CONNECTED) {
                Log.i("tag", "Network Enable");
//                syncAttendance(DataBaseHelper.getInstance().getOfflineAttendanceToSync());
                try {
                    int permissionStorage = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if ((permissionStorage == PackageManager.PERMISSION_GRANTED)) {
                        startIntentServiceForGTS(context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("com.staragri.app.internet.enabled"));
            }
        }
    }

/*    private void syncAttendance(ArrayList<EmpLog> syncList) {
        i = 0;
        synchronizeAttendance(syncList);
    }

    private void synchronizeAttendance(final ArrayList<EmpLog> syncList) {
        if (syncList == null || syncList.isEmpty()) {
            return;
        }
        if (i == syncList.size()) {
            return; //loop is finished;
        }
        String url = Constants.HOST_URL_HTTP + "empLogOfflineMaster.php";
        final EmpLog log = syncList.get(i);
        Map<String, Object> data = new HashMap<>();
        data.put(Constants.USER_DATE, log.getDate());
        data.put(Constants.USER_IN_TIME, log.getIn_time());
        data.put(Constants.USER_OUT_TIME, log.getOut_time());
        data.put(Constants.USER_CHECK_IN_DISTANCE, log.getCheckInDistance());
        data.put(Constants.USER_CHECK_OUT_DISTANCE, log.getCheckOutDistance());
        data.put("employee_code", SharedPreferencesUtil.getValue(Constants.DATA_USER_CODE, ""));
        data.put("wh_pid", log.getWh_pid());
        data.put(Constants.USER_EMP_LOG, DataBaseHelper.getInstance().getUserLocationString(log.getDate()));
        i++;
        HttpService.getInstance().userCheckInOut(url, data, new NetworkCallBackListener(new ResponseListener() {
            @Override
            public void onSuccess(int request_id, Object responseBody) {
                CheckInOut response = (CheckInOut) responseBody;
                if (!response.isError()) {
                    DataBaseHelper.getInstance().setSynchronized(log.getDate());
                    Logger.e("Updated " + log.getDate());
                    synchronizeAttendance(syncList);
                }
            }

            @Override
            public void onError(int request_id, String error_message) {
            }

            @Override
            public void onRequestTimeout() {
            }
        }, false));

    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }*/

    private void startIntentServiceForGTS(Context context) {
        Logger.i("tag","start Service from GPSToggleBroadcastReceiver");
        Intent serviceIntent = new Intent(context, MyService.class);
        try {
            context.startService(serviceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
